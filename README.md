# Archive 2021/11/17

This project is not maintained since 2016.

# New SFML2 Project

Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr <br />
https://gitlab.com/hnc/new_sfml2_project

# Project

This project is a toy project using SFML 2. <br />
Clone it to start your C++ project:

```sh
git clone the_repo_of_your_project
cd the_repo_of_your_project
git remote add new_sfml2_project https://gitlab.com/hnc/new_sfml2_project.git
git remote -v
git pull new_sfml2_project master
# git remote remove new_sfml2_project
# git remote -v
```

### System Requirement

Required:
- SFML 2

### Compilation

###### With CMake

```sh
mkdir build
cd build
cmake ..
make
# make test
./project
```
