// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>

#include <SFML/Graphics.hpp>

#include "../include/main.hpp"


int main()
{
	// Functions from src/main.cpp
	test_function();
	test_function_inline();
	
	std::cout << "Welcome to my project with SFML " << SFML_VERSION_MAJOR << "." << SFML_VERSION_MINOR << "." << SFML_VERSION_PATCH << std::endl;
	
	// Window
	sf::RenderWindow window(sf::VideoMode(800, 600), "Window title");
	
	// Textures
	sf::Texture texture;
	if (texture.loadFromFile("../img/test.png") == false)
	{
		std::cerr << "Error: can not load texture \"../img/test.png\"" << std::endl;
	}
	
	// Sprite
	sf::Sprite sprite;
	sprite.setTexture(texture);
	
	// Time
	sf::Clock clock;
	
	float const speed = 500.f;
	
	while (window.isOpen())
	{
		// Time elapsed
		float time_elapsed = clock.restart().asSeconds();
		
		// Event
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Window closed
			if (event.type == sf::Event::Closed) { window.close(); }
		}
		
		// Move
		float const moving = speed * time_elapsed;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) { sprite.move(0.f, -moving); }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) { sprite.move(0.f, moving); }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) { sprite.move(-moving, 0.f); }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) { sprite.move(moving, 0.f); }
		
		// Clear
		window.clear(sf::Color::Black);
		
		// Draw
		window.draw(sprite);
		
		// Update
		window.display();
	}
	
	return 0;
}
